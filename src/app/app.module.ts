import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import { TableComponent } from './components/table/table.component';
import { Ej2Component } from './components/ej2/ej2.component';

import {RouterModule, Routes} from '@angular/router';

const router: Routes = [

  {
    path: 'ej1',
    component: TableComponent
  },
  {
    path: 'ej2',
    component: Ej2Component
  }
];


@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    Ej2Component
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(router)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
