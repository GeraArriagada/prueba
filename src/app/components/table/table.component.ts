import { Component, OnInit } from '@angular/core';
import { EndpointService } from 'src/app/services/endpoint.service';
import { IResponse } from 'src/app/interfaces/response';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent {
  // definición de variables
  public numbers: number[];
  public noDuplicates: any[];
  public success: boolean;
  public init: boolean;



  constructor(public endpoint: EndpointService) {
    this.numbers = [];
    this.noDuplicates = null;
    this.success = false;
    this.init = false;
  }
  /**
   * metodo que genera la información para construir la tabla
   */
  public build(): void {
    this.init = true;
    this.endpoint.getEndpoint('http://168.232.165.184/prueba/array').subscribe((res: IResponse) => {
      this.numbers = res.data;
      this.success = !res.success;
      this.noDuplicates = [...new Set(this.numbers)];
    });
  }

  /**
   * method to count the times that a certain number appears into a list
   * @param listNumbers numbers list
   * @param toCount number to compare
   */
  public countNumbers(listNumbers: number[], toCount: number): number {
    let count = 0;

    for (const listNumber of listNumbers) {
      if (listNumber === toCount) {
        count++;
      }
    }
    return count;

  }

  /**
   * method to determine the first appearance of a certain number into a list
   * @param list numbers list
   * @param toDet number to compare
   */
  public firstPosition(list: number, toDet: number): number {

    let pos = -1;
    let i = 0;
    while (pos === -1) {
      if (list[i] === toDet) {
        pos = i;
      }
      i++;
    }
    return pos;
  }

  /**
   * method to determine the last appearance of a certain number into a list
   * @param list numbers list
   * @param toDet number to compare
   */
  public lastPosition(list: number[], toDet: number): number {
    let pos: number;
    for (const i in list) {
      if (list[i] === toDet) {
        pos = Number(i);
      }
    }
    return pos;
  }

  /**
   * method to sort a list of numbers from lowest to highest value
   * @param list numbers list
   */
  public sort(list: number[]): number[] {
    let aux = 0;
    for (let i = 0; i < (list.length - 1); i++) {
      for (let j = 0; j < (list.length - i); j++) {
        if (list[j] > list[j + 1]) {
          aux = list[j];
          list[j] = list[j + 1];
          list[j + 1] = aux;
        }
      }
    }
    return list;
  }


}
