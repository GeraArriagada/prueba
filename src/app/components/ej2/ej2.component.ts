import { Component, OnInit } from '@angular/core';
import { EndpointService } from 'src/app/services/endpoint.service';
import { IResponse2 } from 'src/app/interfaces/response2';

@Component({
  selector: 'app-ej2',
  templateUrl: './ej2.component.html',
  styleUrls: ['./ej2.component.css']
})
export class Ej2Component {
  // definición de variables
  public init: boolean;
  public success: boolean;
  public alphabet: string[];
  public paragraph: string[];
  public regex: RegExp;
  public sum: number[];

  // inicializacion de variables y servicios
  constructor(public endpoint: EndpointService) {
    this.init = false;
    this.success = false;
    this.alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');
    this.paragraph = [];
    this.regex = /(\d+)/g;
    this.sum = [];
  }

  /**
   * metodo que genera la información para construir la tabla
   */
  public build(): void {
    let aux = null;
    this.init = true;
    this.endpoint.getEndpoint('http://168.232.165.184/prueba/dict').subscribe((res: IResponse2) => {
      this.success = !res.success;
      const texts = res.data;

      for (const text of texts) {
        aux = text.paragraph.toString();
        this.paragraph.push(aux);
        this.sum.push(this.sumNumbers(aux));
      }
    });
  }

  /**
   * metodo para contar la aparicion de caracteres. Recibe un string y un arreglo de caracteres. Retorna un arreglo de numeros.
   * @param text string to analyze
   * @param endPointArray list of letters to compare
   */
  public countLetters(text: string, endPointArray: string[]): string[] {
    let counter = 0;
    let aux2: any[];
    let aux1: any[];
    aux1 = [];
    aux2 = text.split('');
    for (const endpoint of endPointArray) {
      counter = 0;
      for (const aux of aux2) {
        if (aux === endpoint) {
          counter++;
        }
      }
      aux1.push(counter);
    }
    return aux1;
  }


  /**
   * metodo que suma los valores numericos presentes en un arreglo de strings. Retorna un valor numerico.
   * @param text string to analyze
   */
  public sumNumbers(text: string): number {
    let counter = 0;
    let aux = 0;
    let listNumbers = [];
    listNumbers = text.match(this.regex);
    if (listNumbers != null) {
      for (const listnumber of listNumbers) {
        aux = Number(listnumber);
        counter = counter + aux;
      }
      return counter;
    }
  }

}
