import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EndpointService {
  constructor(private http: HttpClient) { }
  /**
   * method to get data from endpoint
   * @param url url of the endpoint
   */
  public getEndpoint(url: string): Observable<any> {
    return this.http.get(url);
  }
}
