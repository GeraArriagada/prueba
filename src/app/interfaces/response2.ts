export interface IResponse2 {
  data?: IParagraph[];
  error?: any;
  success: boolean;

}

export interface IParagraph {
  paragraph: string;
  number: number;
  hasCopyright: boolean;
}
